unit ufrmInfo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ComCtrls, Vcl.StdCtrls, Vcl.ExtActns;

type
  TfrmInfo = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    edSource: TEdit;
    edTarget: TEdit;
    btnClose: TButton;
    procedure btnCloseClick(Sender: TObject);
  private
  public
    { Public declarations }
  end;

var
  frmInfo: TfrmInfo;

implementation

{$R *.dfm}

procedure TfrmInfo.btnCloseClick(Sender: TObject);
begin
  Close;
end;

end.
