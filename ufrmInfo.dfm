object frmInfo: TfrmInfo
  Left = 0
  Top = 0
  Caption = 'Info'
  ClientHeight = 195
  ClientWidth = 321
  Color = clWhite
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  DesignSize = (
    321
    195)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 52
    Height = 13
    Caption = 'Source File'
  end
  object Label2: TLabel
    Left = 16
    Top = 64
    Width = 51
    Height = 13
    Caption = 'Target File'
  end
  object edSource: TEdit
    Left = 16
    Top = 39
    Width = 297
    Height = 19
    ReadOnly = True
    TabOrder = 0
  end
  object edTarget: TEdit
    Left = 16
    Top = 83
    Width = 297
    Height = 19
    ReadOnly = True
    TabOrder = 1
  end
  object btnClose: TButton
    Left = 240
    Top = 154
    Width = 73
    Height = 33
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    TabOrder = 2
    OnClick = btnCloseClick
  end
end
