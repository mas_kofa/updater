program updater;

uses
  Vcl.Forms,
  ufrmInfo in 'ufrmInfo.pas' {frmInfo},
  ufrmUtama in 'ufrmUtama.pas' {frmUtama};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmUtama, frmUtama);
  Application.CreateForm(TfrmInfo, frmInfo);
  Application.Run;
end.
